package com.myhealth1st.MedicrossPatientCheckIn;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class MyConstants {

	static void showAlert(Activity xyz, String title, String message,
			String btnname) {
		new AlertDialog.Builder(xyz)
				.setTitle(title)
				.setMessage(message)
				.setInverseBackgroundForced(true)
				.setNeutralButton(btnname,
						new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {

							}

						}).show();
	}
}